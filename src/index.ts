import './css/style.scss'
import * as d3 from 'd3'

const data_url = 'https://raw.githubusercontent.com/freeCodeCamp/ProjectReferenceData/master/GDP-data.json'
const width = 800,
  height = 400,
  yMargin = 50,
  xMargin = 50,
  padding = 20;

d3.json(data_url).then(function ({name, data}) {
  const years: Date[] = data.map(([date]: [string]) => new Date(date))
  const barWidth = Math.floor(width / years.length)
  const xScale = d3.scaleTime()
    .domain([d3.min(years), d3.max(years)])
    .range([0, width - yMargin - padding])

  const gdp: [number] = data.map((([_, g]: [string, number]) => g))
  const yScale = d3.scaleLinear()
    .domain([0, d3.max(gdp)])
    .range([0, height - yMargin - padding])

  d3.select('#title')
    .text(name)

  const graph = d3.select('#graph')
    .append('svg')
    .attr('width', width)
    .attr('height', height)

  const xAxis = d3.axisBottom(xScale)
  graph.append('g')
    .call(xAxis)
    .attr('id', 'x-axis')
    .attr('transform', 'translate(' + xMargin + ', ' + (height - yMargin) + ')')

  const invertYScale = d3.scaleLinear()
    .domain([0, d3.max(gdp)])
    .range([height - yMargin - padding, 0])
  const yAxis = d3.axisLeft(invertYScale)
  graph.append('g')
    .call(yAxis)
    .attr('id', 'y-axis')
    .attr('transform', 'translate(' + xMargin + ', ' + padding + ')')
  graph.append('text')
    .attr('transform', 'rotate(-90)')
    .attr('x', -(yMargin + padding))
    .attr('y', xMargin + padding)
    .text('GDP')

  const tooltip = d3.select('main')
    .append('div')
    .attr('id', 'tooltip')

  graph.selectAll('rect')
    .data(data)
    .enter()
    .append('rect')
    .attr('fill', '#0cad77')
    .attr('class', 'bar')
    .attr('data-date', ([date, _]) => date)
    .attr('data-gdp', ([_, gdp]) => gdp)
    .attr('y', ([_, gdp]) => height - yMargin - yScale(gdp))
    .attr('x', (_, i) => xMargin + xScale(years[i]))
    .attr('width', barWidth)
    .attr('height', ([_, gdp]) => yScale(gdp))
    .on('mouseover', tooltipMouseover)

  function tooltipMouseover(data: [string, number]) {
    const nonGraphPad = Math.abs((window.innerWidth - width) / 2);
    tooltip.style('opacity', 1)
      .attr('data-date', this.dataset.date)
      .attr('data-gdp', this.dataset.gdp)
      .style('left', (xScale(new Date(data[0])) + nonGraphPad) + 'px')
      .style('top', (width / 3) + 'px')
      .html(`${this.dataset.date} - ${this.dataset.gdp}`)
  }
})
